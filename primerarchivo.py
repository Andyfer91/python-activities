# hacer un algoritmo que permita calcular el area de
# un triangulo
# se necesita, base, altura y devuelve area

base = int(input("Ingrese base del triangulo: "))
altura = int(input("Ingrese altura del triangulo: "))
unidades = input("Ingrese unidades de los datos suministrados: ")
area = (altura * base) / 2
print("El area del triangulo es: ", area + unidades)

# Segundo programa
# Hacer un algoritmo que permita evaluar si un numero es par o no
# Ingresa un dato por consola, devuelve "par" o "impar"

NumEvaluado = int(input("Ingrese el numero entero a evaluar: "))
if NumEvaluado % 2 == 0:
    print("el numero", NumEvaluado, "es par")
else:
    print("El numero ", NumEvaluado, "es impar")

# Tercer programa
# a un numero de 2 digitos iguales, calcularle la suma de los digitos iguales o anteriores a n

nVariable = int(input("Ingrese el valor n de la variable\n"
                      "no debe ser mayor de 2 digitos: "))
if 0 > nVariable > 99:
    print("Valor no valido")
# Validar el numero
validacion = nVariable // 10
validacion2 = nVariable % 10

if validacion == validacion2:
    suma = (validacion * (validacion + 1)) / 2
    print("La suma de los enteros del numero es: ", suma)

else:
    print("El valor esta dentro del rango pero no cumple \n"
          "las condiciones de la suma")

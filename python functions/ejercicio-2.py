# Ingresar por consola una cantidad x de segundos
# devolver, el equivalente en años, meses, dias, horas, minutos y
# segundos
#

def residuo(self):
    self2 = self % 60
    self = (self - self2) / 60
    return self2, self


def residuohoras(self):
    self2 = self % 24
    self = (self - self2) / 24
    return self2, self


def residuodias(self):
    self2 = self % 30
    self = (self - self2) / 30
    return self2, self


def residuofinal(self):
    self2 = self % 12
    self = (self - self2) / 12
    return self2, self


def ex_2_4(self):
    seg, self = residuo(self)
    min, self = residuo(self)
    hour, self = residuohoras(self)
    days, self = residuodias(self)
    months, year = residuofinal(self)
    dict = {'años': year, 'meses': months, 'dias': days,
            'horas': hour, 'min': min, 'seg': seg}
    print(dict)


tiempo = 36660000001
ex_2_4(tiempo)

import re
import random


# Crea una dificultad del juego, decide que tantas vidas pierdes por error
def dificult():
    texto = """
    1. Facil
    2. Intermedio
    3. Dificil
    4. One Shot
    Elija dificultad:  """
    try:
        self = int(input(texto))
        while 0 < self > 4:
            self = int(input(texto))
        if self == 1:
            return 3
        elif self == 2:
            return 4
        elif self == 3:
            return 6
        elif self == 4:
            return 18
    except ValueError:
        return dificult()


# Crea una cadena de guiones equivalente a la longitud de la palabra
def letrasaguion(self):
    otra = ""
    for self in range(len(self)):
        otra = otra + "_"
    otra = (" ".join(otra))
    return otra


# lee una letra, compara si existe la letra en la palabra, reemplaza la letra
# en la cadena de guiones
def cambioletraguion(self, palabra, vidas, dificulta):
    try:
        change = 0
        self = (" ".join(self))
        palabra = list(palabra)
        self = list(self)
        guion = palabra
        letra = leerletra()
        for palabra in range(len(palabra)):
            if letra == self[palabra]:
                guion[palabra] = self[palabra]
                change = change + 1
        if change == 0:
            vidas = vidas + dificulta

        self = ("".join(self))
        guion = ("".join(guion))
    except ValueError:
        print('ingrese un valor valido')
    return self, guion, vidas


# Cambia en funcion de la vida del usuario
def vidausuario(self):
    ahorcado = """  
  |
 O 
/ | \ 
/  \ 
 """
    return ahorcado[:self]


# Elige una palabra al azar de la lista
def aleatoreidad():
    list = ['adivina', 'palabra', 'efectivo', 'pycharm', 'reactivo', 'requisito']
    word = random.choice(list)
    return word


# valida la letra que va a buscar el usuario
def leerletra():
    letra = str(input('ingrese letra: '))
    while letra.isalpha() == False or len(letra) > 1 or len(letra) < 1:
        letra = str(input("valor invalido, ingrese letra: "))
    return letra


# permite rejugar el juego o terminarlo
def repetir():
    repetir = input('Juego Terminado \n'
                    'has ganado\n'
                    'Si desea terminar, envie n ')
    if repetir == 'n':
        return False
    else:
        return True


if __name__ == '__main__':

    ciclo = True
    while ciclo == True:
        tofind = aleatoreidad()
        dificultad = dificult()
        lives = 6
        publish = vidausuario(lives)
        show = letrasaguion(tofind)
        while lives < 24 and show != (" ".join(tofind)):
            palabra, show, lives = cambioletraguion(tofind, show, lives, dificultad)
            publish = vidausuario(lives)
            print(show, publish)
        if lives >= 24:
            print('juego Terminado \n'
                  'la palabra era: ', tofind)
            ciclo = repetir()
        elif show == (" ".join(tofind)):
            ciclo = repetir()
            # Termina el programa

# Escribir una función que calcule el total de una factura tras aplicarle el
# IVA. La función debe recibir la cantidad sin IVA y el porcentaje de IVA
# a aplicar, y devolver el total de la factura. Si se invoca la función
# sin pasarle el porcentaje de IVA, deberá aplicar un 21%.

def calculadoraiva():
    try: # valida que el valor ingresado sea un numero
        self = float(input('ingrese cantidad a calcular: '))
    except ValueError:
        print('ingrese un valor numerico')
    try:
        iva = float(input('ingrese valor del iva(%0-100): '))
    except ValueError:
        resultado = self * (0.21 + 1)
        return print(resultado)
    else:
        iva = 1 / iva
        resultado = self * (iva + 1)
        return print(resultado)

calculadoraiva()

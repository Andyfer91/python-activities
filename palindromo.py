# Solicitar una cadena de caracteres al usuario, comparar si la palabra
# se lee igual de izquierda a derecha y viceversa y decirle al usuario
# si la condicion se cumple o no

word = input("ingrese una palabra: ")
inverseword = word
inverseword = inverseword[::-1]

if word.lower() != inverseword.lower():
    print("no es palindromo")
else:
    print("Es palindromo")

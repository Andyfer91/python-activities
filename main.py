test = "hello world"
print(test[0:5])
# Imprime los caracteres contenidos en esa lista
print(test[0::3])
# imprime las cada cierta numero de posiciones
print(test[::-1])
# imprime la cadena invertida
list = list()
# metodo list() crea una lista []
print(list)
list.append(3)
print(list)
_list = [1, 1, 2, 2, 3, 4, 4, 5, 6, 6, 6, 7, 8, 9]
print(set(_list))
# metodo set convierte en conjunto, una lista
print(f' {test} e')
print('{}e'.format(test))
print('{1}e'.format(test, 5))
print('h' in test) #no imprime h, sino el valor booleano



import openpyxl
import re
import pprint


def is_email(email):
    regex = r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])"
    try:
        return re.match(regex, email) is not None
    except TypeError:
        pass

def is_id(row_id):
    return isinstance(row_id, int)


def is_sales(sales):
    return isinstance(sales, float)

class ReadFile:

    def __init__(self, path):
        self.path = path

    def read_file(self):
        wb_obj = openpyxl.load_workbook(self.path)
        sheet_obj = wb_obj.active
        first_row = []

        for col in range(sheet_obj.max_column):
            first_row.append(sheet_obj.cell(1, col + 1).value)
        data = []

        for row in range(2, sheet_obj.max_row + 1):
            elm = {}
            for col in range(sheet_obj.max_column):
                elm[first_row[col]] = sheet_obj.cell(row, col + 1).value
                data.append(elm)
        return data

    def listas_clean_data(self, data):
        clean_data =[]
        for items in data:
            email =is_email(items.get('email'))
            row_id =is_id(items.get('row_id'))
            sales = is_sales(items.get('sales'))
            if email and row_id and sales:
                clean_data.append(items)
        return (clean_data)

    def listas_trash_data(self, data):
        trash_data =[]
        for items in data:
            email =is_email(items.get('email'))
            row_id =is_id(items.get('row_id'))
            sales = is_sales(items.get('sales'))
            if not email  or not row_id or not sales:
                trash_data.append(items)
        return (trash_data)

    def validar_ship_mode(self, data):
        ship_mode = {}
        for items in data:
            if ship_mode.get(items.get('ship_mode')) and items.get('ship_mode') is not None:
                ship_mode.get(items.get('ship_mode')).append(items)
            else:
                ship_mode[items.get('ship_mode')] = [items]
        return ship_mode


def main():
    link = ReadFile('/home/lsv/Escritorio/Actividades Python/Excel python/archivo-papython.xlsx')
    print('lista limpia:\n \n', len(link.listas_clean_data(link.read_file())))
    #print('lista sucia:\n \n',  link.listas_trash_data(link.read_file()))
    #print('Ship mode:\n \n', link.validar_ship_mode(link.read_file()))

if __name__ == '__main__':
    main()
